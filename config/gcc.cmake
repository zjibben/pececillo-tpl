# Generic Linux with GNU C/C++/Fortran

set(ENABLE_MPI NO CACHE BOOL "Parallel libraries")
set(ENABLE_OPENMP NO CACHE BOOL "Use OpenMP")
set(CMAKE_BUILD_TYPE Release CACHE STRING "Build type")
set(CMAKE_C_COMPILER gcc CACHE STRING "C Compiler")
